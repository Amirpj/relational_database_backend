/**
* SQL: Intro
*
*/

-- DDL - Data Definition Language
-- CREATE, ALTER, DROP

DROP TABLE post CASCADE CONSTRAINTS;
DROP TABLE cuser CASCADE CONSTRAINTS;
DROP TABLE ccomment CASCADE CONSTRAINTS;

DROP SEQUENCE seq_user;
DROP SEQUENCE seq_post;
DROP SEQUENCE seq_comment;

CREATE TABLE cuser (
    user_id_pk INT PRIMARY KEY,
    username VARCHAR2(45) NOT NULL,
    email VARCHAR2(255) UNIQUE NOT NULL,
    gender VARCHAR2(1),
    password VARCHAR2(45) NOT NULL,
    birthday DATE NULL,
    CONSTRAINT check_gender CHECK (UPPER(gender) = 'M' OR UPPER(gender) = 'F')
);

CREATE TABLE post (
        post_id INT PRIMARY KEY,
        title VARCHAR2(45) NOT NULL,
        posttext VARCHAR2(2048) NULL,
        picture VARCHAR2(45) NULL,
        created_at DATE DEFAULT SYSDATE,
        user_id INT NOT NULL,
        CONSTRAINT fk_user FOREIGN KEY (user_id)
            REFERENCES cuser(user_id_pk)
);

CREATE TABLE ccomment (
    comment_id INT PRIMARY KEY,
    comment_text VARCHAR2(2048) NOT NULL,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    CONSTRAINT fk_user_c FOREIGN KEY (user_id)
        REFERENCES cuser(user_id_pk),
    CONSTRAINT fk_post_c FOREIGN KEY (post_id)
        REFERENCES post(post_id)
);

CREATE SEQUENCE seq_user;
CREATE SEQUENCE seq_post;
CREATE SEQUENCE seq_comment;


--DML : DATA MANIPULATION Language
-- INSERT INTO, UPDATE, DELETE

--INSERT INTO <table_name> (<column1>, <column2>, ..)
--VALUES (<value1>, <value2>, ..)
-- wrong:
--INSERT INTO cuser (user_id_pk, username) VALUES (1,'asdf');

INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'peter', 'peter@mail.com', 'M', '12345');
INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'peter_duplicate', 'peter2@mail.com', 'M', '12345');
INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'susi', 'susi@mail.com', 'F', '12345');
INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'hans', 'hans@mail.com', 'M', '12345');
INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'gertrud', 'gertrud@mail.com', 'F', '12345');
INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'ludwig', 'ludwig@mail.com', 'M', '12345');
INSERT INTO cuser (user_id_pk, username, email, gender, password)
VALUES (seq_user.NEXTVAL, 'maria', 'maria@mail.com', 'F', '12345');

INSERT INTO post (post_id, title, posttext, picture, user_id)
VALUES (seq_post.NEXTVAL, 'Hello world', 'asdfasdfasdf', 'img/picture.jpg', 1);
--BLOB = Binary Large Object

INSERT INTO ccomment(comment_id, comment_text, user_id, post_id)
VALUES (seq_comment.NEXTVAL, 'Hey, great post..', 1, 1);

COMMIT;
-- ROLLBACK;

--UPDATE <table> SET <col>=<value>, ...
--WHERE <condition>

UPDATE cuser SET username = 'Peter3';
ROLLBACK;

UPDATE cuser SET username = 'Peter3' WHERE user_id_pk = 2;
COMMIT;

--DELETE FROM <table> WHERE <condition>;
DELETE FROM cuser WHERE email = 'peter2@mail.com';
COMMIT;

-- try it:
DELETE FROM cuser WHERE user_id_pk = 1;



--ALTER TABLE post ADD (updated_at DATE NULL);

/*
UPPER('asdf') > ASDF
LOWER('ASDF') > asdf
*/
-- DML
-- INSERT, UPDATE, DELETE
/*INSERT INTO cuser (user_id, username, email, gender)
VALUES (1, 'Peter', 'peter@someting.com', 'M');
INSERT INTO cuser (user_id, username, email, gender)
VALUES (2, 'Erroruser', 'error@someting.com', 'X');
INSERT INTO cuser (user_id, username, email, gender)
VALUES (3, 'Susi', 'susi@someting.com', 'f');
INSERT INTO cuser (user_id, username, email, gender)
VALUES (3, 'Susi', 'susi@someting.com', UPPER('f'));
INSERT INTO cuser (user_id, username, email, gender)
VALUES (4, 'Peter2', 'peter@someting.com', 'M');

UPDATE cuser SET email='susinew@somethingelse.com'
WHERE user_id=3;

DELETE cuser WHERE username IS LIKE 'P%';

-- DQL
-- SELECT
SELECT username, email FROM cusers;
SELECT username || '- EMAIL:'|| email FROM cusers;
*/
