/**
* Movie Database Application
* from the course Relational Databases
*
* @version: 0.1
* @date: 2020-12-18
* @author: Damian Gawenda
*/


-- droping tables
DROP TABLE genre CASCADE CONSTRAINTS;
DROP TABLE language CASCADE CONSTRAINTS;
DROP TABLE movie CASCADE CONSTRAINTS;
DROP TABLE muser CASCADE CONSTRAINTS;
DROP TABLE playlist CASCADE CONSTRAINTS;
DROP TABLE playlist_item CASCADE CONSTRAINTS;
DROP TABLE rating CASCADE CONSTRAINTS;
DROP TABLE translation CASCADE CONSTRAINTS;

-- droping sequences
DROP SEQUENCE seq_translation;
DROP SEQUENCE seq_pl_item;
DROP SEQUENCE seq_rating;
DROP SEQUENCE seq_user;
DROP SEQUENCE seq_language;
DROP SEQUENCE seq_movie;
DROP SEQUENCE seq_genre;
DROP SEQUENCE seq_playlist;

-- creating tables
CREATE TABLE genre (
    genre_id INT PRIMARY KEY,
    genre_name VARCHAR2(45) NOT NULL
);

CREATE TABLE language (
    language_id INT PRIMARY KEY,
    iso_code VARCHAR2(2) NOT NULL
);

CREATE TABLE movie (
    movie_id INT PRIMARY KEY,
    title VARCHAR2(45) NOT NULL,
    year_of_release INT NOT NULL,
    trailer_link VARCHAR2(255) NULL,
    netflix_link VARCHAR2(255) NULL,
    amazon_link VARCHAR2(255) NULL,
    genre_id INT NOT NULL,
    language_id INT NOT NULL, -- the original language
    CONSTRAINT fk_genre FOREIGN KEY (genre_id) REFERENCES genre(genre_id),
    CONSTRAINT fk_lang FOREIGN KEY (language_id) REFERENCES language(language_id)
);

CREATE TABLE muser (
    user_id  INT PRIMARY KEY,
    username VARCHAR2(45) NOT NULL,
    password VARCHAR2(45) NOT NULL,
    gender VARCHAR2(1) NULL,
    birthday DATE NULL,
    email VARCHAR2(255) NOT NULL, -- unique
    CONSTRAINT uq_email UNIQUE (email)
);

CREATE TABLE playlist (
    playlist_id INT PRIMARY KEY,
    created_at DATE DEFAULT SYSDATE,
    pl_name  VARCHAR2(45) NOT NULL,
    user_id INT NOT NULL,
    CONSTRAINT fk_user_pl FOREIGN KEY (user_id) REFERENCES muser(user_id)
);

CREATE TABLE rating (
    rating_id INT PRIMARY KEY,
    rate INT NOT NULL,
    created_at DATE DEFAULT SYSDATE,
    user_id INT NOT NULL,
    movie_id INT NOT NULL,
    CONSTRAINT fk_user_rating FOREIGN KEY (user_id) REFERENCES muser(user_id),
    CONSTRAINT fk_movie_rating FOREIGN KEY (movie_id) REFERENCES movie(movie_id),
    CONSTRAINT ck_rate CHECK (rate > 0 AND rate <= 5) -- CHECK (rate IN (1,2,3,4,5))
);

CREATE TABLE playlist_item (
    playlist_item_id INT PRIMARY KEY,
    movie_id INT NOT NULL,
    playlist_id INT NOT NULL,
    sortorder INT NULL,
    CONSTRAINT fk_playlist FOREIGN KEY (playlist_id) REFERENCES playlist(playlist_id),
    CONSTRAINT fk_movie_i FOREIGN KEY (movie_id) REFERENCES movie(movie_id)
);

CREATE TABLE translation (
    translation_id  INT PRIMARY KEY,
    movie_id INT NOT NULL,
    language_id INT NOT NULL,
    CONSTRAINT fk_movie_transl FOREIGN KEY (movie_id) REFERENCES movie(movie_id),
    CONSTRAINT fk_lang_transl FOREIGN KEY (language_id) REFERENCES language(language_id)
);

-- create sequences
CREATE SEQUENCE seq_translation;
CREATE SEQUENCE seq_pl_item;
CREATE SEQUENCE seq_rating;
CREATE SEQUENCE seq_user;
CREATE SEQUENCE seq_language;
CREATE SEQUENCE seq_movie;
CREATE SEQUENCE seq_genre;
CREATE SEQUENCE seq_playlist;


-- insert Data
INSERT INTO genre (genre_id, genre_name) VALUES (seq_genre.NEXTVAL, 'Comedy');
INSERT INTO genre (genre_id, genre_name) VALUES (seq_genre.NEXTVAL, 'Triller');
INSERT INTO genre (genre_id, genre_name) VALUES (seq_genre.NEXTVAL, 'Romance');
INSERT INTO genre (genre_id, genre_name) VALUES (seq_genre.NEXTVAL, 'Sci-Fi');

INSERT INTO muser (user_id, username, password, gender, birthday, email)
VALUES (seq_user.NEXTVAL, 'Peter', '12345', 'M', TO_DATE('1990-01-01', 'YYYY-MM-DD'), 'peter@email.com');
INSERT INTO muser (user_id, username, password, gender, birthday, email)
VALUES (seq_user.NEXTVAL, 'Susi', '12345', 'F', TO_DATE('1990-01-01', 'YYYY-MM-DD'), 'susir@email.com');
INSERT INTO muser (user_id, username, password, gender, birthday, email)
VALUES (seq_user.NEXTVAL, 'Hans', '12345', 'M', TO_DATE('1990-01-01', 'YYYY-MM-DD'), 'hans@email.com');

INSERT INTO language (language_id, iso_code) VALUES (seq_language.NEXTVAL, 'DE');
INSERT INTO language (language_id, iso_code) VALUES (seq_language.NEXTVAL, 'EN');
INSERT INTO language (language_id, iso_code) VALUES (seq_language.NEXTVAL, 'FR');
INSERT INTO language (language_id, iso_code) VALUES (seq_language.NEXTVAL, 'HI');

INSERT INTO movie (movie_id, title, year_of_release, trailer_link, netflix_link,amazon_link,genre_id,language_id)
VALUES (seq_movie.NEXTVAL, 'Matrix', 1999, 'http://youtube.com/watch..', 'netflix..', 'amazon', 4, 2);
INSERT INTO movie (movie_id, title, year_of_release, trailer_link, netflix_link,amazon_link,genre_id,language_id)
VALUES (seq_movie.NEXTVAL, 'Matrix2', 2005, 'http://youtube.com/watch..', 'netflix..', 'amazon', 4, 2);
INSERT INTO movie (movie_id, title, year_of_release, trailer_link, netflix_link,amazon_link,genre_id,language_id)
VALUES (seq_movie.NEXTVAL, '3 Idiots', 2005, 'http://youtube.com/watch..', 'netflix..', 'amazon', 1, 4);


INSERT INTO playlist (playlist_id, pl_name, user_id) VALUES (seq_playlist.NEXTVAL, 'my fav movies', 1);

INSERT INTO playlist_item (playlist_item_id, playlist_id, movie_id, sortorder)
VALUES (seq_pl_item.NEXTVAL, 1, 1, 1);
INSERT INTO playlist_item (playlist_item_id, playlist_id, movie_id, sortorder)
VALUES (seq_pl_item.NEXTVAL, 1, 2, 2);

INSERT INTO translation (translation_id, language_id, movie_id)
VALUES (seq_translation.NEXTVAL, 1, 1);
INSERT INTO translation (translation_id, language_id, movie_id)
VALUES (seq_translation.NEXTVAL, 3, 1);

INSERT INTO rating (rating_id, rate, user_id, movie_id)
VALUES (seq_rating.NEXTVAL, 5, 1, 1);
INSERT INTO rating (rating_id, rate, user_id, movie_id)
VALUES (seq_rating.NEXTVAL, 3, 1, 2);
INSERT INTO rating (rating_id, rate, user_id, movie_id)
VALUES (seq_rating.NEXTVAL, 4, 2, 1);
INSERT INTO rating (rating_id, rate, user_id, movie_id)
VALUES (seq_rating.NEXTVAL, 1, 2, 2);

COMMIT; -- ends the transaction

-- query examples from the use cases:

-- SELECT <columns>
-- FROM <tables>
-- WHERE <connection or conditions>
-- ORDER BY ...
-- GROUP BY ...
-- HAVING ...

SELECT * FROM movie;

SELECT title, year_of_release, trailer_link
FROM movie, rating
WHERE movie.movie_id = rating.movie_id
ORDER BY rating.rate DESC;

SELECT title, year_of_release, trailer_link, avg(rating.rate)
FROM movie, rating
WHERE movie.movie_id = rating.movie_id
GROUP by title, year_of_release, trailer_link
ORDER BY avg(rating.rate) DESC;

-- 'MATR' > outputs the movies with this title
-- 'Sci-Fi' > outputs the movies that have this genre
-- 'EN'     > outputs all movies that have this language oringally / next step with translations.


SELECT title, movie_id
FROM movie, genre, language
WHERE movie.genre_id = genre.genre_id
AND  movie.language_id = language.language_id
AND (
    movie.title LIKE 'Mat%' OR genre.genre_name LIKE 'Mat%' OR language.iso_code LIKE 'Mat%'
);

-- _______________ < search form
-- value goes into a variable on the application server
-- value goes into the SQL-VARIABLE <var>

SELECT title, movie_id
FROM movie, genre, language
WHERE movie.genre_id = genre.genre_id
AND  movie.language_id = language.language_id
AND (
    movie.title LIKE '<var>%' OR genre.genre_name LIKE '<var>%' OR language.iso_code LIKE '<var>%'
);
