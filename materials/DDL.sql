/*
* Movie Database 2019
*/

/* DDL: Data Definition Language */

/* CREATE, ALTER, DROP */
/*5 CONSTRAINTS (RULES):
primary key
foreign key
null/not null
UNIQUE
CHECK (e.g. CHECK(birthday < NOW AND deathday is NULL))
*/


/*CREATE TABLE name_of_the_table (
    column1 datatype,
    column2 datatype,
);*/

DROP TABLE play;
DROP TABLE actor;
DROP TABLE movie;
DROP TABLE genre;


CREATE TABLE actor (
    actor_id INT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    birthday DATE NULL,
    origin VARCHAR(45) NULL,
    CONSTRAINT bday_check CHECK (birthday < TO_DATE('2019-11-29','YYYY-MM-DD'))
);
CREATE TABLE genre (
    genre_id INT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE movie (
    movie_id INT PRIMARY KEY,
    title  VARCHAR(255) NOT NULL,
    rating INT NULL,
    year INT NOT NULL,
    duration INT NULL,
    language VARCHAR(2) NULL,
    genre_id INT NOT NULL,
    CONSTRAINT fk_genre FOREIGN KEY (genre_id)
    REFERENCES genre(genre_id),
    CONSTRAINT rating_check CHECK (rating >=1 AND rating<=5)
);

CREATE TABLE play (
    play_id INT PRIMARY KEY,
    rolename VARCHAR(45) NOT NULL,
    actor_id INT NOT NULL,
    movie_id INT NOT NULL,
    CONSTRAINT fk_actor FOREIGN KEY (actor_id)
    REFERENCES actor(actor_id),
    CONSTRAINT fk_movie FOREIGN KEY (movie_id)
    REFERENCES movie(movie_id)
);

INSERT INTO actor (actor_id, name)
VALUES (1,'Keanu Reaves');
INSERT INTO genre (genre_id, name)
VALUES (1,'Action');
/*testing rating constraint:*/
INSERT INTO movie (movie_id, title, rating, year, genre_id)
VALUES (1,'Matrix', 7, 2019, 1);
INSERT INTO movie (movie_id, title, rating, year, genre_id)
VALUES (1,'Matrix', 5, 2019, 1);
