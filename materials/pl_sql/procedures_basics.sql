/**
* PL/SQL - Procedures Basics
*/

-- activates the output for DBMS_OUTPUT
SET SERVEROUTPUT ON;

--DROP PROCEDURE proc_hello_world;
CREATE OR REPLACE PROCEDURE proc_hello_world
IS
-- optional variables
BEGIN
    --action
    DBMS_OUTPUT.PUT_LINE('Hello World');
END;
/
SHOW ERRORS;

--execute PROCEDURE
EXECUTE proc_hello_world;



CREATE OR REPLACE PROCEDURE proc_hello_with_params(p_name VARCHAR2, p_lang VARCHAR)
IS
    v_output VARCHAR2(255);
    v_helloDE VARCHAR(11) := 'Hallo';
    v_helloEN VARCHAR(11) := 'Hello';
    v_helloFR VARCHAR(11) := 'Salut';
BEGIN
    IF p_lang = 'DE' THEN
        v_output := v_helloDE || ' '|| p_name;
    ELSIF p_lang = 'EN' THEN
        v_output := v_helloEN || ' '|| p_name;
    ELSIF p_lang = 'FR' THEN
        v_output := v_helloFR || ' '|| p_name;
    ELSE
        v_output := 'Language not available';
    END IF;
    DBMS_OUTPUT.PUT_LINE(v_output);
END;
/
SHOW ERRORS;

EXECUTE proc_hello_with_params('Peter', 'EN');
EXECUTE proc_hello_with_params('Hans', 'DE');
EXECUTE proc_hello_with_params('Jaque', 'FR');
EXECUTE proc_hello_with_params('Nobody', 'XX');




DROP TABLE languages;
CREATE TABLE languages (
  language VARCHAR2(2) PRIMARY KEY,
  greeting VARCHAR2(32) NOT NULL
);
INSERT INTO languages VALUES ('DE','Hallo');
INSERT INTO languages VALUES ('EN','Hello');
INSERT INTO languages VALUES ('FR','Salut');
INSERT INTO languages VALUES ('ES','Hola');
COMMIT;


CREATE OR REPLACE PROCEDURE proc_hello_table_access(p_name VARCHAR2, p_lang VARCHAR)
IS
    v_hello VARCHAR2(11);
BEGIN
    SELECT greeting INTO v_hello FROM languages WHERE language = p_lang;
    DBMS_OUTPUT.PUT_LINE(v_hello || ' ' || p_name);
    EXCEPTION
    WHEN no_data_found THEN
        DBMS_OUTPUT.PUT_LINE('Language not available');
END;
/
SHOW ERRORS;

EXECUTE proc_hello_table_access('Peter', 'EN');
EXECUTE proc_hello_table_access('Hans', 'DE');
EXECUTE proc_hello_table_access('Jaque', 'FR');
EXECUTE proc_hello_table_access('Nobody', 'XX');
