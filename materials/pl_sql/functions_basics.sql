/**
* PL/SQL - Functions Basics
*/

CREATE OR REPLACE FUNCTION func_hello_world
RETURN VARCHAR2
IS
-- optional variables
BEGIN
    -- Action
    RETURN('Hello World');
END;
/
SHOW ERRORS;

-- call the function
SELECT func_hello_world FROM DUAL; -- dual is a placeholder table


CREATE OR REPLACE FUNCTION func_hello_with_params(p_name VARCHAR2)
RETURN VARCHAR2
IS
 v_output VARCHAR2(45);
BEGIN
    -- Action
    v_output := 'Hello ' || p_name;
    RETURN v_output;
END;
/
SHOW ERRORS;

-- call the function
SELECT func_hello_with_params('Peter') FROM DUAL;




CREATE OR REPLACE FUNCTION func_calc_goldenratio(p_input NUMBER)
RETURN NUMBER
IS
    v_result NUMBER(10,5);
BEGIN
    -- the formular for the golden ratio: https://en.wikipedia.org/wiki/Golden_ratio
    v_result := p_input / ( (1+SQRT(5)) / 2 );
    RETURN v_result;
END;
/
SHOW ERRORS;

-- test
SELECT func_calc_goldenratio(1) FROM DUAL;
SELECT func_calc_goldenratio(2) FROM DUAL;
SELECT func_calc_goldenratio(33) FROM DUAL;
SELECT func_calc_goldenratio(100) FROM DUAL;


-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-- receipie -> name, preparation, valuation
-- usage -> ingredient_id

drop table recipe;
drop table usage;
drop table ingredient;

create table recipe (
    recipe_id integer not null primary key,
    name varchar2(50),
    preparation varchar2(80),
    image varchar2(255),
    time_of_preparation date
);

create table ingredient (
    ingredient_id integer primary key,
    name varchar2(50),
    image varchar2(255)
);

create table usage (
    recipe_id integer,
    ingredient_id integer,
    amount varchar2(50),
    constraint fkey1 foreign key (recipe_id) references recipe(recipe_id),
    constraint fkey2 foreign key (ingredient_id) references ingredient(ingredient_id)
);


insert into recipe (recipe_id, name, preparation, image, time_of_preparation)
values (1001, 'burger', 'blah bnlah', 'image link burger 1', TO_DATE('02/12/2021', 'DD/MM/YYYY'));

delete from recipe where recipe_id=1002;

insert into recipe (recipe_id, name, preparation, image, time_of_preparation)
values (1002, 'pizza', 'blah bnlah', 'image link pizza 1', TO_DATE('01/12/2021', 'DD/MM/YYYY'));

select * from recipe;

insert into ingredient (ingredient_id, name, image)
values (2001, 'cheese', 'image link cheese 1');

insert into ingredient (ingredient_id, name, image)
values (2002, 'salami', 'image link salami 1');

select * from ingredient;

insert into usage (recipe_id, ingredient_id, amount)
values (1001, 2001, '100g');

insert into usage (recipe_id, ingredient_id, amount)
values (1001, 2002, '3 slices');

insert into usage (recipe_id, ingredient_id, amount)
values (1002, 2001, '150g');

-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------



CREATE OR REPLACE FUNCTION generate_random_number(p_in NUMBER)
RETURN NUMBER
IS
    v_result: NUMBER(10, 5);

BEGIN
    v_result := some_random_number(p_in);
    RETURN v_result;
END

SELECT generate_random_number(12) FROM DUAL;


create or replace function whatever_fname(p_in varchar2)
return varchar2
is
v_o varchar2(100);
begin
    v_o := p_in || ' test';
    return v_o;
end;
/
show errors;

select whatever_fname('1212') from dual;