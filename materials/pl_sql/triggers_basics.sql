/**
* PL/SQL - Triggers Basics
*/

SET SERVEROUTPUT ON;

-- Events: AFTER/BEFORE INSERT/UPDATE/DELETE
-- Transition Variables: OLD, NEW

DROP TABLE test_table;
CREATE TABLE test_table (col VARCHAR2(45));

CREATE OR REPLACE TRIGGER trg_test
AFTER INSERT ON test_table
FOR EACH ROW
BEGIN
    -- Action
    DBMS_OUTPUT.PUT_LINE('Trigger fired..');
    DBMS_OUTPUT.PUT_LINE(:NEW.col || ' inserted');
END;
/
SHOW ERRORS;

-- test / "execute" the trigger
INSERT INTO test_table VALUES ('some value');
INSERT INTO test_table VALUES ('some more value');
INSERT INTO test_table VALUES ('and even more');
COMMIT;





DROP TABLE log_table;
CREATE TABLE log_table (log VARCHAR2(255));

CREATE OR REPLACE TRIGGER trg_logging
AFTER INSERT OR UPDATE OR DELETE ON test_table
FOR EACH ROW
DECLARE v_logtext VARCHAR2(255);
BEGIN
    CASE
        WHEN INSERTING THEN
            v_logtext := 'the following content was added: ' ||:NEW.col;
        WHEN UPDATING THEN
            v_logtext := 'the following content was updated from '|| :OLD.col||' to : ' ||:NEW.col;
        WHEN DELETING THEN
            v_logtext := 'the following content was deleted: ' ||:OLD.col;
    END CASE;
    INSERT INTO log_table VALUES (v_logtext);
END;
/
SHOW ERRORS;

-- test
INSERT INTO test_table VALUES ('some value for trigger2');
UPDATE test_table SET col = 'some changes for trigger2';
DELETE FROM test_table;

select * from log_table;
