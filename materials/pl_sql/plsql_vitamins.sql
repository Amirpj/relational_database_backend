/**
* PLSQL - Vitamines
*/

DROP TABLE orders CASCADE CONSTRAINTS;
DROP TABLE vitamine CASCADE CONSTRAINTS;
DROP SEQUENCE seq_vitamine;
DROP SEQUENCE seq_order;

CREATE TABLE vitamine(
    vitamin_id INTEGER PRIMARY KEY,
    name VARCHAR2(45) NOT NULL,
    in_stock INTEGER NOT NULL,
    min_stock INTEGER NOT NULL
);

CREATE TABLE orders (
    order_id INTEGER PRIMARY KEY,
    vitamin_id INTEGER NOT NULL,
    amount INTEGER NOT NULL,
    ordertime DATE NOT NULL,
    CONSTRAINT vit_fk FOREIGN KEY (vitamin_id) REFERENCES vitamine(vitamin_id)
);

CREATE SEQUENCE seq_vitamine;
CREATE SEQUENCE seq_order;

-- sample data:
INSERT INTO vitamine VALUES (seq_vitamine.NEXTVAL, 'Vitamin C', 100, 10);
INSERT INTO vitamine VALUES (seq_vitamine.NEXTVAL, 'Vitamin D', 100, 10);
INSERT INTO vitamine VALUES (seq_vitamine.NEXTVAL, 'Vitamin B12', 100, 10);
COMMIT;


CREATE TRIGGER trg_auto_order
AFTER UPDATE ON vitamine
FOR EACH ROW
BEGIN
    IF :NEW.in_stock < :NEW.min_stock  THEN
        --INSERT INTO orders VALUES (seq_order.NEXTVAL, :NEW.vitamin_id, 100, SYSDATE);
        proc_ordering(:NEW.vitamin_id);
    END IF;
END;
/
SHOW ERRORS;
/

CREATE OR REPLACE PROCEDURE proc_ordering (p_vitamine_id NUMBER)
IS
BEGIN
    INSERT INTO orders VALUES (seq_order.NEXTVAL, p_vitamine_id, 100, SYSDATE);
END;
/
SHOW ERRORS;
/

-- test
UPDATE vitamine SET in_stock = 9 WHERE vitamin_id=1;
UPDATE vitamine SET in_stock = 9 WHERE vitamin_id=2;
UPDATE vitamine SET in_stock = 9 WHERE vitamin_id=3;

--UPDATE vitamine SET in_stock = (SELECT )

/*
CREATE OR REPLACE TRIGGER trg_auto_order_prozess
AFTER INSERT ON orders
FOR EACH ROW
BEGIN
    UPDATE vitamine SET in_stock = in_stock + :NEW.amount WHERE vitamin_id=:NEW.vitamin_id;
END;
/
SHOW ERRORS;
/
*/
COMMIT;

-- check data
SELECT * FROM ORDERS;
SELECT * FROM vitamine;


CREATE TRIGGER trg_auto_update_order_number
AFTER INSERT ON orders
FOR EACH ROW
EXECUTE update_total_orders
/
SHOW ERRORS;
/


CREATE OR REPLACE FUNCTION update_total_orders()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	UPDATE customers 
	SET "Total Orders" = "Total Orders" + 1 
	WHERE _id = :NEW.customer_id
END;
$$