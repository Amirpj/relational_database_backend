const connectToDb = require('./connectToDb');
const { getErrorResponse, getSuccessResponse } = require('./helpers/reponse');

module.exports.getAllCustomers = async () => {
    const query = `SELECT _id, CONCAT("First Name", ' ', "Last Name") as name, email, address1 as address, city FROM customers;`;
    const pool = connectToDb();
    if(pool) {
        try {
            const customerResponse = await pool.query(query);
            if(customerResponse) {
                // console.log(">>>>>>>>>>",customerResponse);
                if(customerResponse.rows.length > 0) {
                    return getSuccessResponse(customerResponse.rows);
                } else {
                    return getSuccessResponse('no data available');
                }
            } else{
                return getErrorResponse(new Error(`reponse empty`));
            }
        } catch (err) {
            return getErrorResponse(err)
        }   
    }
}

module.exports.addNewCustomer = async (event) => {

    console.log("event.body: ", event.body, typeof(event.body))
    const customer = JSON.parse(event.body);
    const {first_name, last_name, email, address, city, province, country} = customer;
    
    const query = `INSERT INTO customers ("First Name", "Last Name", "email", "address1", "city", "province", "country") 
    VALUES ('${first_name}','${last_name}','${email}', '${address}', '${city}', '${province}', '${country}');`

    console.log(query)
    const pool = connectToDb()

    if(pool) {
        try {
            const addNewCustomerReponse = await pool.query(query);
            if(addNewCustomerReponse) {
                return getSuccessResponse(addNewCustomerReponse);
            }
        } catch(err) {
            return getErrorResponse(err);
        }
    }
}

module.exports.findOneCustomer = async (event) => {

    console.log("event.queryStringParameters: ", event.queryStringParameters, typeof(event.queryStringParameters));
    const { customerId } = event.queryStringParameters;
    console.log("customerId: ", customerId);
    
    const query = `SELECT "First Name" as f_name, "Last Name" as l_name, email, company, address1 as address, city, country, phone, "Total Spent" as total_spent, "Total Orders" as total_orders FROM customers where "_id" = '${customerId}'`;

    const pool = connectToDb()

    if(pool) {
        try {
            const findOneCustomerReponse = await pool.query(query);
            if(findOneCustomerReponse) {
                return getSuccessResponse(findOneCustomerReponse.rows);
            }
        } catch(err) {
            return getErrorResponse(err);
        }
    }
}

module.exports.deleteCustomer = async (event) => {

    console.log("event.body: ", event.body, typeof(event.body))
    const customer = JSON.parse(event.body);
    console.log("customer");
    const { name } = customer;
    const firstName = name.split(" ")[0];
    const lastName = name.split(" ")[1];
    
    const query = `DELETE FROM customers where "First Name" = '${firstName}' AND "Last Name" = '${lastName}';`;

    console.log(query)
    const pool = connectToDb()

    if(pool) {
        try {
            const deleteCustomerReponse = await pool.query(query);
            if(deleteCustomerReponse) {
                return getSuccessResponse(deleteCustomerReponse);
            }
        } catch(err) {
            return getErrorResponse(err);
        }
    }
}