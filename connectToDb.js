const Pool = require('pg').Pool;
const dbConfig = require('./config/db');


const connectToDb = () => {
    const pool = new Pool(dbConfig);

    return pool
}

module.exports = connectToDb;
