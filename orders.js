const connectToDb = require('./connectToDb');
const { getErrorResponse, getSuccessResponse } = require('./helpers/reponse');

module.exports.getAllOrders = async () => {
    const query = `SELECT _id, Name as name, email, "Financial Status" as financial_status, "Paid at" as paid_at FROM orders`;
    const pool = connectToDb();
    if(pool) {
        try {
            const ordersResponse = await pool.query(query)
            if(ordersResponse) {
                // console.log(">>>>>>>>>>",ordersResponse);
                if(ordersResponse.rows.length > 0) {
                    return getSuccessResponse(ordersResponse.rows);
                } else {
                    return getSuccessResponse('no data available');
                }
            } else{
                return getErrorResponse(new Error(`reponse empty`));
            }
        } catch (err) {
            return getErrorResponse(err)
        }   
    }
}

module.exports.fetchOrdersForCustomer = async (event) => {

    console.log("event.queryStringParameters: ", event.queryStringParameters, typeof(event.queryStringParameters));
    const { customerId } = event.queryStringParameters;
    console.log("customerId: ", customerId);

    const query = `SELECT _id, Name as name, email, "Financial Status" as financial_status, "Paid at" as paid_at FROM orders WHERE customer_id='${customerId}'`;
    const pool = connectToDb();
    if(pool) {
        try {
            const ordersResponse = await pool.query(query)
            if(ordersResponse) {
                // console.log(">>>>>>>>>>",ordersResponse);
                if(ordersResponse.rows.length > 0) {
                    return getSuccessResponse(ordersResponse.rows);
                } else {
                    return getSuccessResponse('no data available');
                }
            } else{
                return getErrorResponse(new Error(`reponse empty`));
            }
        } catch (err) {
            return getErrorResponse(err)
        }   
    }
}

module.exports.findOneOrder = async (event) => {

    console.log("event.queryStringParameters: ", event.queryStringParameters, typeof(event.queryStringParameters));
    const { orderId } = event.queryStringParameters;
    console.log("orderId: ", orderId);
    
    const query = `SELECT name, email, "Financial Status" as financial_status, "Paid at" as paid_at, total as amount, "Created at" as created_at FROM orders WHERE "_id" = '${orderId}'`;

    const pool = connectToDb();

    if(pool) {
        try {
            const findOneCustomerReponse = await pool.query(query);
            if(findOneCustomerReponse) {
                return getSuccessResponse(findOneCustomerReponse.rows);
            }
        } catch(err) {
            return getErrorResponse(err);
        }
    }
}

module.exports.placeOrder = async (event) => {

    console.log("event.body: ", event.body, typeof(event.body))
    const order = JSON.parse(event.body);
    console.log("order: ", order);

    const { name, email, financial_status, paid_at, order_amount_total, customer_id, order_product } = order;
    
    const query = `INSERT INTO orders (Name, email, "Financial Status", "Paid at", total, customer_id, product_id) 
    VALUES ('${name}','${email}','${financial_status}', '${paid_at}', ${parseFloat(order_amount_total).toFixed(2)}, '${customer_id}', '${order_product}');`
    // const query = `SELECT "Total Orders", email FROM customers WHERE _id = '${customer_id}';`

    console.log(query)
    const pool = connectToDb()

    if(pool) {
        try {
            const placeOrderReponse = await pool.query(query);
            if(placeOrderReponse) {
                console.log(">>>>>> placeOrderReponse: ", placeOrderReponse);
                // const query = `SELECT "Total Orders", email FROM customers WHERE _id = ${customer_id}`
                // const query = `UPDATE customers SET "Total Orders" = "Total Orders" + 1 where customer_id = ${customer_id}` 
                return getSuccessResponse(placeOrderReponse);
            }
        } catch(err) {
            return getErrorResponse(err);
        }
    }
}