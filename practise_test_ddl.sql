-- CREATE A DATABASE

DROP DATABASE harringtonapp;

CREATE DATABASE harringtonapp
    WITH 
    OWNER = master
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE harringtonapp
    IS 'default administrative connection database';
	
-- CREATING TABLES
-- FOR CUSTOMER	

DROP TABLE customers;
DROP SEQUENCE seq_customer;

CREATE SEQUENCE seq_customer;

CREATE TABLE customers (
	"First Name" VARCHAR(60),
	"Last Name" VARCHAR(60),
	Email VARCHAR(150),
	Company VARCHAR(60),
	Address1 VARCHAR(120),
	Address2 VARCHAR(120),
	City VARCHAR(50),
	Province VARCHAR(120),
	"Provinde Code" VARCHAR(120),
	Country VARCHAR(50),
	"Country Code" VARCHAR(30),
	Zip VARCHAR(60),
	Phone VARCHAR(20),
	"Accepts Marketing" VARCHAR(120),
	"Total Spent" VARCHAR(120),
	"Total Orders" VARCHAR(120),
	Tags VARCHAR(255),
	Note VARCHAR(255),
	"Tax Exempt" VARCHAR(60) 
);

ALTER TABLE customers
ADD ID UUID DEFAULT uuid_generate_v4 () PRIMARY KEY;

SELECT * FROM customers;

INSERT INTO customers ("First Name", "Last Name", Email, Company, Phone, City, Country, Zip)
VALUES ('Adesh', 'Shrestha', 'depab74018@geeky83.com', '', '984-3935114', 'Lalitpur', 'Nepal', 44600);

SELECT "First Name", "Last Name", Email, Company FROM customers;


-- FOR ORDERS

DROP TABLE orders;

CREATE TABLE orders (
	Name VARCHAR(60),
	Email VARCHAR(150),
	"Financial Status" VARCHAR(60),
	"Paid at" VARCHAR(60),
	"Fulfillment Status" VARCHAR(120),
	"Fulfilled at" VARCHAR(120),
    "Accepts Marketing" VARCHAR(120),
	Currency VARCHAR(20),
	Subtotal VARCHAR(20),
	Shipping VARCHAR(30),
    Taxes VARCHAR(30),
	Total VARCHAR(30),
	"Discount Amount" VARCHAR(40),
	"Shippping Method" VARCHAR(40),
	"Created at" VARCHAR(30),
	"Lineitem quantity" VARCHAR(30),
    "Lineitem name" VARCHAR(120), 
    "Lineitem price" VARCHAR(30),
    "Lineitem compare at price" VARCHAR(40),
    "Lineitem sku" VARCHAR(40),
    "Lineitem requires shipping" VARCHAR(40),
    "Lineitem taxable" VARCHAR(40),
    "Lineitem fulfillment status" VARCHAR(40),
    "Billing Name" VARCHAR(120),
    "Billing Street" VARCHAR(200),
    "Billing Address1" VARCHAR(255),
	"Billing Address2" VARCHAR(255),
    "Billing City" VARCHAR(60),
    "Billing Zip" VARCHAR(60),
    "Billing Country" VARCHAR(80),
    "Billing Phone" VARCHAR(80),
    "Payment Method" VARCHAR(100),
    Id VARCHAR(100),
    Phone VARCHAR(60)
);

ALTER TABLE orders
DROP customer_id;

ALTER TABLE orders
ADD customer_id uuid;

ALTER TABLE orders ADD CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers(_id);

ALTER TABLE orders
DROP product_id;

ALTER TABLE orders
ADD product_id uuid;

ALTER TABLE orders ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products(_id);

SELECT * FROM orders;

SELECT Name, Email, "Payment Method" FROM orders;


-- FOR PRODUCTS

DROP TABLE products;

CREATE TABLE products (
	Handle VARCHAR(100),
    Title VARCHAR(255),
    "Body (HTML)" VARCHAR(2048),
    Vendor VARCHAR(80),
    Type VARCHAR(60),
    Tags VARCHAR(60),
    Published VARCHAR(60),
    "Option1 Name" VARCHAR(60),
    "Option1 Value" VARCHAR(60),
    "Option2 Name" VARCHAR(60),
    "Option2 Value" VARCHAR(60),
    "Variant SKU" VARCHAR(80),
    "Variant Grams" VARCHAR(60),
    "Variant Inventory Tracker" VARCHAR(60),
    "Variant Inventory Qty" VARCHAR(30),
    "Variant Inventory Policy" VARCHAR(30),
    "Variant Fulfillment Service" VARCHAR(30),
    "Variant Price" VARCHAR(60),
    "Variant Compare At Price" VARCHAR(60),
    "Variant Requires Shipping" VARCHAR(30),
    "Variant Taxable" VARCHAR(80),
    "Image Src" VARCHAR(255),
    "Image Position" VARCHAR(60),
    "Gift Card" VARCHAR(60),
    "Variant Image" VARCHAR(255),
    "Variant Weight Unit" VARCHAR(60),
    "Status" VARCHAR(60)
);

SELECT * FROM products;

ALTER TABLE products
DROP orders_id;

ALTER TABLE products
ADD order_id uuid;

ALTER TABLE products ADD CONSTRAINT fk_order_id FOREIGN KEY (order_id) REFERENCES orders(_id);


-- FOR COLLECTIONS

CREATE TABLE collections (
	Tags VARCHAR(255),
	Title VARCHAR(255),
    "Body (HTML)" VARCHAR(2048),
	Published VARCHAR(60),
	product_id uuid,
	CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products(_id));

-- ######################## DDL End #######################################
-- ######################## DDL End #######################################

-- Queries to add to the application
-- ######################## DDL End #######################################

-- Make filters with -- 
SELECT * FROM customers
WHERE CustomerName LIKE "s%";

SELECT * FROM Customers
WHERE NOT Country='Germany' AND NOT Country='USA';

SELECT * FROM customers
WHERE Country="UK" AND City="London" OR Country="Germany" AND City="Berlin";

SELECT * FROM Customers
ORDER BY Country ASC, CustomerName DESC;

SELECT c.customername, c.address, c.postalcode, o.orderid, o.orderdate  
FROM customers AS c, orders AS o
WHERE c.customerid=o.customerid;

SELECT SupplierName
FROM Suppliers
WHERE EXISTS (SELECT ProductName FROM Products WHERE Products.SupplierID = Suppliers.supplierID AND Price < 20); 

CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CONSTRAINT PK_Person PRIMARY KEY (ID,LastName)
);

-- BEGIN TRANSACTION;
-- UPDATE orders


SELECT * FROM orders;
SELECT * FROM customers;
SELECT "Total Orders", email FROM customers WHERE _id = 'a1719610-0c93-426b-bcce-29f6e08600e1';

ALTER TABLE customers 
ALTER COLUMN "Total Orders" TYPE int
USING "Total Orders"::integer;

ALTER TABLE customers 
ALTER COLUMN "Total Spent" TYPE numeric
USING "Total Spent"::NUMERIC;

ALTER TABLE orders 
ALTER COLUMN total TYPE numeric
USING total::NUMERIC;

DELETE FROM orders WHERE customer_id = 'a1719610-0c93-426b-bcce-29f6e08600e1';
DELETE FROM orders WHERE email = 'Email';
DELETE FROM customers WHERE email = 'Email';

CREATE OR REPLACE FUNCTION update_total_orders()
RETURNS TRIGGER 
AS
$$
BEGIN
	UPDATE customers 
	SET 
		"Total Orders" = "Total Orders" + 1,  
		"Total Spent" = "Total Spent" + NEW.total
	WHERE _id = NEW.customer_id;
	
	RETURN NULL;
END
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER trg_auto_update_order_number
AFTER INSERT ON orders
FOR EACH ROW
EXECUTE PROCEDURE update_total_orders();

DROP TRIGGER trg_auto_update_order_number ON orders;





