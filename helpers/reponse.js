const getErrorResponse = (error) => {
  console.log(error);
  return {
    statusCode: error.statusCode || 500,
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({ message: error.message, success: false }),
  };
};

 const getSuccessResponse = (response) => {
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      message: 'Executed Successfully',
      data: response,
      success: true,
    }),
  };
};


module.exports = {
    getErrorResponse,
    getSuccessResponse
}