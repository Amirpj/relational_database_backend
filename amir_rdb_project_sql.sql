-- ######################## DDL START #######################################
-- CREATE A DATABASE

DROP DATABASE harringtonapp;

CREATE DATABASE harringtonapp
    WITH 
    OWNER = master
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE harringtonapp
    IS 'default administrative connection database';

-- DROP TABLEs

DROP TABLE customers;
DROP TABLE orders;
DROP TABLE products;
DROP TABLE collections;

-- CREATE TABLEs

CREATE TABLE customers (
	"First Name" VARCHAR(60),
	"Last Name" VARCHAR(60),
	Email VARCHAR(150),
	Company VARCHAR(60),
	Address1 VARCHAR(120),
	Address2 VARCHAR(120),
	City VARCHAR(50),
	Province VARCHAR(120),
	"Provinde Code" VARCHAR(120),
	Country VARCHAR(50),
	"Country Code" VARCHAR(30),
	Zip VARCHAR(60),
	Phone VARCHAR(20),
	"Accepts Marketing" VARCHAR(120),
	"Total Spent" VARCHAR(120),
	"Total Orders" VARCHAR(120),
	Tags VARCHAR(255),
	Note VARCHAR(255),
	"Tax Exempt" VARCHAR(60) 
);

CREATE TABLE orders (
	Name VARCHAR(60),
	Email VARCHAR(150),
	"Financial Status" VARCHAR(60),
	"Paid at" VARCHAR(60),
	"Fulfillment Status" VARCHAR(120),
	"Fulfilled at" VARCHAR(120),
    "Accepts Marketing" VARCHAR(120),
	Currency VARCHAR(20),
	Subtotal VARCHAR(20),
	Shipping VARCHAR(30),
    Taxes VARCHAR(30),
	Total VARCHAR(30),
	"Discount Amount" VARCHAR(40),
	"Shippping Method" VARCHAR(40),
	"Created at" VARCHAR(30),
	"Lineitem quantity" VARCHAR(30),
    "Lineitem name" VARCHAR(120), 
    "Lineitem price" VARCHAR(30),
    "Lineitem compare at price" VARCHAR(40),
    "Lineitem sku" VARCHAR(40),
    "Lineitem requires shipping" VARCHAR(40),
    "Lineitem taxable" VARCHAR(40),
    "Lineitem fulfillment status" VARCHAR(40),
    "Billing Name" VARCHAR(120),
    "Billing Street" VARCHAR(200),
    "Billing Address1" VARCHAR(255),
	"Billing Address2" VARCHAR(255),
    "Billing City" VARCHAR(60),
    "Billing Zip" VARCHAR(60),
    "Billing Country" VARCHAR(80),
    "Billing Phone" VARCHAR(80),
    "Payment Method" VARCHAR(100),
    Id VARCHAR(100),
    Phone VARCHAR(60)
);

CREATE TABLE products (
	Handle VARCHAR(100),
    Title VARCHAR(255),
    "Body (HTML)" VARCHAR(2048),
    Vendor VARCHAR(80),
    Type VARCHAR(60),
    Tags VARCHAR(60),
    Published VARCHAR(60),
    "Option1 Name" VARCHAR(60),
    "Option1 Value" VARCHAR(60),
    "Option2 Name" VARCHAR(60),
    "Option2 Value" VARCHAR(60),
    "Variant SKU" VARCHAR(80),
    "Variant Grams" VARCHAR(60),
    "Variant Inventory Tracker" VARCHAR(60),
    "Variant Inventory Qty" VARCHAR(30),
    "Variant Inventory Policy" VARCHAR(30),
    "Variant Fulfillment Service" VARCHAR(30),
    "Variant Price" VARCHAR(60),
    "Variant Compare At Price" VARCHAR(60),
    "Variant Requires Shipping" VARCHAR(30),
    "Variant Taxable" VARCHAR(80),
    "Image Src" VARCHAR(255),
    "Image Position" VARCHAR(60),
    "Gift Card" VARCHAR(60),
    "Variant Image" VARCHAR(255),
    "Variant Weight Unit" VARCHAR(60),
    "Status" VARCHAR(60)
);

CREATE TABLE collections (
	Tags VARCHAR(255),
	Title VARCHAR(255),
    "Body (HTML)" VARCHAR(2048),
	Published VARCHAR(60),
	product_id uuid,
	CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products(_id));

ALTER TABLE customers
ADD ID UUID DEFAULT uuid_generate_v4 () PRIMARY KEY;

ALTER TABLE orders
ADD customer_id uuid;
ALTER TABLE orders ADD CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers(_id);
ALTER TABLE orders
ADD product_id uuid;
ALTER TABLE orders ADD CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products(_id);
ALTER TABLE products
ADD order_id uuid;

ALTER TABLE products ADD CONSTRAINT fk_order_id FOREIGN KEY (order_id) REFERENCES orders(_id);
ALTER TABLE customers 
ALTER COLUMN "Total Orders" TYPE int
USING "Total Orders"::integer;

ALTER TABLE customers 
ALTER COLUMN "Total Spent" TYPE numeric
USING "Total Spent"::NUMERIC;

ALTER TABLE orders 
ALTER COLUMN total TYPE numeric
USING total::NUMERIC;


-- ######################## DDL END #######################################



-- ######################## DML START #######################################
const query = `INSERT INTO customers ("First Name", "Last Name", "email", "address1", "city", "province", "country") 
    VALUES ('Amir','Shrestha','amir@test.com', 'Am Sandberg', 'Gummersbach', 'NRW', 'Germany');`
VALUES ('Adesh', 'Shrestha', 'depab74018@geeky83.com', '', '984-3935114', 'Lalitpur', 'Nepal', 44600);
DELETE FROM customers where "First Name" = 'Amir' AND "Last Name" = 'Shrestha';
UPDATE customers SET "First Name" = 'Amir pj' WHERE _id='a1719610-0c93-426b-bcce-29f6e08600e1';
-- ######################## DML END #######################################

-- ######################## DQL START #######################################
SELECT "First Name" as f_name, "Last Name" as l_name, email, company, address1 as address, city, country, phone, "Total Spent" as total_spent, "Total Orders" as total_orders FROM customers where "_id" = 'a1719610-0c93-426b-bcce-29f6e08600e1';
SELECT _id, CONCAT("First Name", ' ', "Last Name") as name, email, address1 as address, city FROM customers;
SELECT handle, title, "Body (HTML)" as description, "Variant SKU" as sku, "Variant Inventory Qty" as quantity, "Variant Image" as image_url FROM products where "_id" = '18d1ccc5-efb7-485e-be23-a64eaa8fb3d8';
SELECT title, "Body (HTML)" as description, tags FROM collections LIMIT 50;
SELECT _id, Name as name, email, "Financial Status" as financial_status, "Paid at" as paid_at FROM orders WHERE customer_id='a1719610-0c93-426b-bcce-29f6e08600e1';
-- ######################## DQL END #######################################



-- ######################## PL SQL START #######################################

CREATE OR REPLACE FUNCTION update_total_orders()
RETURNS TRIGGER 
AS
$$
BEGIN
	UPDATE customers 
	SET 
		"Total Orders" = "Total Orders" + 1,  
		"Total Spent" = "Total Spent" + NEW.total
	WHERE _id = NEW.customer_id;
	
	RETURN NULL;
END
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER trg_auto_update_order_number
AFTER INSERT ON orders
FOR EACH ROW
EXECUTE PROCEDURE update_total_orders();

DROP TRIGGER trg_auto_update_order_number ON orders;

-- ######################## PL SQL START #######################################
