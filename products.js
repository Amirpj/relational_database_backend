const connectToDb = require('./connectToDb');
const { getErrorResponse, getSuccessResponse } = require('./helpers/reponse');

module.exports.getAllProducts = async () => {
    const query = `SELECT _id, title, handle, "Body (HTML)" as description, "Variant SKU" as sku FROM products LIMIT 50;`;
    const pool = connectToDb();
    if(pool) {
        try {
            const productsResponse = await pool.query(query)
            if(productsResponse) {
                // console.log(">>>>>>>>>>",productsResponse);
                if(productsResponse.rows.length > 0) {
                    return getSuccessResponse(productsResponse.rows);
                } else {
                    return getSuccessResponse('no data available');
                }
            } else{
                return getErrorResponse(new Error(`reponse empty`));
            }
        } catch (err) {
            return getErrorResponse(err)
        }   
    }
}

module.exports.findOneProduct = async (event) => {

    console.log("event.queryStringParameters: ", event.queryStringParameters, typeof(event.queryStringParameters));
    const { productId } = event.queryStringParameters;
    console.log("productId: ", productId);
    
    const query = `SELECT handle, title, "Body (HTML)" as description, "Variant SKU" as sku, "Variant Inventory Qty" as quantity, "Variant Image" as image_url FROM products where "_id" = '${productId}'`;

    const pool = connectToDb();

    if(pool) {
        try {
            const findOneCustomerReponse = await pool.query(query);
            if(findOneCustomerReponse) {
                return getSuccessResponse(findOneCustomerReponse.rows);
            }
        } catch(err) {
            return getErrorResponse(err);
        }
    }
}