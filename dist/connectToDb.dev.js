"use strict";

var Pool = require('pg').Pool;

var dbConfig = require('./config/db');

var connectToDb = function connectToDb() {
  var pool = new Pool(dbConfig);
  return pool;
};

module.exports = connectToDb;