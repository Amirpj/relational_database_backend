"use strict";

module.exports.getAllTodos = function (event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  db.getAll('todo').then(function (res) {
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(res)
    });
  })["catch"](function (e) {
    console.log(e);
    callback(null, {
      statusCode: e.statusCode || 500,
      body: 'Error: Could not find Todos: ' + e
    });
  });
};

module.exports.createTodo = function (event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  var data = JSON.parse(event.body);
  db.insert('todo', data).then(function (res) {
    callback(null, {
      statusCode: 200,
      body: "Todo Created!" + res
    });
  })["catch"](function (e) {
    callback(null, {
      statusCode: e.statusCode || 500,
      body: "Could not create Todo " + e
    });
  });
};

module.exports.updateTodo = function (event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  var data = JSON.parse(event.body);
  db.updateById('todo', event.pathParameters.id, data).then(function (res) {
    callback(null, {
      statusCode: 200,
      body: "Todo Updated!" + res
    });
  })["catch"](function (e) {
    callback(null, {
      statusCode: e.statusCode || 500,
      body: "Could not update Todo" + e
    });
  });
};

module.exports.deleteTodo = function (event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  db.deleteById('todo', event.pathParameters.id).then(function (res) {
    callback(null, {
      statusCode: 200,
      body: "Todo Deleted!"
    });
  })["catch"](function (e) {
    callback(null, {
      statusCode: e.statusCode || 500,
      body: "Could not delete Todo" + e
    });
  });
};