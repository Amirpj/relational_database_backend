const connectToDb = require('./connectToDb');
const { getErrorResponse, getSuccessResponse } = require('./helpers/reponse');

module.exports.getAllCollections = async () => {
    const query = `SELECT title, "Body (HTML)" as description, tags FROM collections LIMIT 50;`;
    const pool = connectToDb();
    if(pool) {
        try {
            const collectionsResponse = await pool.query(query)
            if(collectionsResponse) {
                // console.log(">>>>>>>>>>",collectionsResponse);
                if(collectionsResponse.rows.length > 0) {
                    return getSuccessResponse(collectionsResponse.rows);
                } else {
                    return getSuccessResponse('no data available');
                }
            } else{
                return getErrorResponse(new Error(`reponse empty`));
            }
        } catch (err) {
            return getErrorResponse(err)
        }   
    }
}

// module.exports.addCustomers = async ()